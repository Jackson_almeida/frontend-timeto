import { Observable } from 'rxjs';
import { Injectable, NgModule } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {

    var token = localStorage.getItem("Authorization")!;

    const dupReq = req.clone({
      headers: req.headers.set("Authorization", token ? 'Bearer ' + token : ''),
    });

    return next.handle(dupReq);
  }
}

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsRequestInterceptor,
      multi: true
    },
  ],
})

export class Interceptor { }
