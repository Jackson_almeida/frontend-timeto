export interface Event {
  name: string,
  description: string,
  initialDatetime: string|null,
  finalDatetime: string|null
}

