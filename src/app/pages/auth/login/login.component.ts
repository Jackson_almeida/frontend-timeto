import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './login.service';
import { AlertModalService } from 'src/app/shared/alert-modal/alert-modal.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  validator(): boolean {
    if (this.loginForm.status === "VALID") return true
    else return false
  }

  onSubmit() {
    if (this.validator()) {
      this.loginService.create(this.loginForm.value).subscribe(
        success => {
          localStorage.clear();
          localStorage.setItem('Authorization', Object.values(success)[0]);
          this.router.navigate(['/']);
          this.alertService.showAlertSuccess("Login efetudado com sucesso!");
        },
        () => this.alertService.showAlertDanger("Dados invalidos, tente novamente!"),
      );
    } else {
      this.alertService.showAlertWarning('Preencha os campos!')
    }
  }

  constructor(
    private loginService: LoginService,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertModalService
  ) {
    this.loginForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

}
