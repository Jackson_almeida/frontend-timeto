import { AlertModalService } from 'src/app/shared/alert-modal/alert-modal.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  checkPasswordequality = (password: any, confirmPassword: any): boolean => password === confirmPassword ? true : false;

  onSubmit() {
    const checkPassword = this.checkPasswordequality(
      this.registerForm.get("password")?.value,
      this.registerForm.get("confirmPassword")?.value
    )

    if (this.registerForm.status == "VALID" && checkPassword) {
      this.registerService.create(this.registerForm.value).subscribe(
        success => {
          this.router.navigate(['/auth/login']);
          this.alertService.showAlertInfo('Cadastro realizado com sucesso, efetue o login!');
        },
        error => this.alertService.showAlertDanger("Ocorreu um erro inesperado, tente novamente!"),
      )
    } else {
      this.alertService.showAlertWarning('ATENÇÃO: Há algum campo incorreto!');
    }
  }

  constructor(
    private registerService: RegisterService,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertModalService
  ) {
    this.registerForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength]],
      confirmPassword: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

}
