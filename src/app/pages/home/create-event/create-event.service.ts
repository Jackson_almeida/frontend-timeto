import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { EventListService } from '../event-list/event-list.service';



@Injectable({
  providedIn: 'root',
})
export class CreateEventService {

  constructor(private http: HttpClient) { }

  private readonly API = `${environment.backend}/events`;

  token : any = ''

  create(events: any) {
    return this.http.post(this.API, events);
  }
}
