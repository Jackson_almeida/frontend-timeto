import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CreateEventService } from './create-event.service';
import { Router } from '@angular/router';
import { Event } from '../../../interfaces/event.interface';
import { AlertModalService } from 'src/app/shared/alert-modal/alert-modal.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  createEventForm: FormGroup;

  formatDatetime = (date: string, time: any): string|null => {
    if (date != null && time != null) return `${date} ${time}`
    else return null
  };

  validator(event: Event): boolean {
    if (this.createEventForm.status == "VALID" && (event.initialDatetime && event.finalDatetime != null) &&
       (new Date(event.finalDatetime) >= new Date(event.initialDatetime))) return true;
    else return false;
  }

  onSubmit(): any {
    const eventObj = {
      name: this.createEventForm.get('name')?.value,
      description: this.createEventForm.get('description')?.value,
      initialDatetime: this.formatDatetime(this.createEventForm.get('initialDate')?.value, this.createEventForm.get('initialTime')?.value),
      finalDatetime: this.formatDatetime(this.createEventForm.get('finalDate')?.value, this.createEventForm.get('finalTime')?.value)
    }

    if (this.validator(eventObj)) {
      return this.createEventService.create(eventObj).subscribe(
        () => {
          this.router.navigate(["/event-list"]);
          this.alertService.showAlertSuccess("Evento criado com sucesso!");
        },
        () => this.alertService.showAlertDanger("Ocorreu um erro inesperado, tente novamente!")
      );
    } else {
      this.alertService.showAlertWarning("OPS! Algum campo inválido!");
    }
  }

  constructor(
    private createEventService: CreateEventService,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertModalService
  ) {
    this.createEventForm = this.formBuilder.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      initialDate: [null, Validators.required],
      initialTime: [null, Validators.required],
      finalDate: [null, Validators.required],
      finalTime: [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }

}
