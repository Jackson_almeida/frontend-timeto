import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class EventListService {

  private readonly API = `${environment.backend}/events`;

  token : any = ''

  constructor(private http: HttpClient) { }

  getEvents() {
    return this.http.get(this.API);
  }

  saveEdit(id: number, event: any) {
    console.log("Cheguei")
    return this.http.post(`${this.API}/${id}`, event);
  }

  deleteEventId(eventId : number) {
    return this.http.delete(`${this.API}/${eventId}`);
  }


}
