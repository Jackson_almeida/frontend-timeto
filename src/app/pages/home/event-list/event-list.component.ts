import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventListService } from './event-list.service';
import { Event } from 'src/app/interfaces/event.interface';
import { AlertModalService } from 'src/app/shared/alert-modal/alert-modal.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  modalForm: FormGroup;
  modalDeleteEvent: FormGroup;

  arrayEvents: any = [];

  private currentModalRef: NgbModalRef | undefined;
  private currentDeleteModalRef: NgbModalRef | undefined;

  getEvents(): any {
    this.eventListService.getEvents().subscribe(
      success => {
        this.arrayEvents = success;
      },
      error => error
    )
  }

  formatDatetime = (date: string, time: any): string|null => {
    if (date != null && time != null) return `${date} ${time}`
    else return null
  };

  validator(event: Event): boolean {
    console.log(this.modalForm.status)
    if (this.modalForm.status == "VALID" && (event.initialDatetime && event.finalDatetime != null) &&
       (new Date(event.finalDatetime) >= new Date(event.initialDatetime))) return true;
    else return false;
  }

  saveEdit(): void{
    const eventObj = {
      name: this.modalForm.get('modalName')?.value,
      description: this.modalForm.get('modalDescription')?.value,
      initialDatetime: this.formatDatetime(this.modalForm.get('modalInitialDate')?.value, this.modalForm.get('modalInitialTime')?.value),
      finalDatetime: this.formatDatetime(this.modalForm.get('modalFinalDate')?.value, this.modalForm.get('modalFinalTime')?.value)
    }

    if (this.validator(eventObj)) {
      this.eventListService.saveEdit(this.modalForm.get('modalEventId')?.value, eventObj).subscribe(
        success => {
          this.currentModalRef?.close();
          this.getEvents();
          this.alertService.showAlertSuccess("Evento editado com sucesso!");
        },
        error => {
          console.error("ERROR: ", error);
          this.alertService.showAlertDanger("Ocorreu um erro inesperado, tente novamente!");
        }
    );
    } else {
      this.alertService.showAlertWarning("OPS! Algum campo inválido!");
    }

  }

  deleteEvent(id: any): void {
    this.eventListService.deleteEventId(id).subscribe(
      () => {
        this.getEvents();
        this.currentModalRef?.close();
        this.alertService.showAlertSuccess("Evento removido com sucesso!");
      },
      () => this.alertService.showAlertDanger("Ocerreu um problema ao deletar, tente novamente!")
    );
  }

  getDate(date: Date) {
    const dateTime = new Date(date);
    let objectDateAndTime = {
      date: `${dateTime.getFullYear()}-${dateTime.getMonth()+1 < 10 ? "0" + (dateTime.getMonth() + 1): dateTime.getMonth()+1}-${dateTime.getDate() < 10 ? "0" + dateTime.getDate() : dateTime.getDate()}`,
      time: `${dateTime.getHours()}:${dateTime.getMinutes() < 10 ? "0" + dateTime.getMinutes() : dateTime.getMinutes()}`
    }
    return objectDateAndTime;
  }

  openModal(content: any, item: any) {
    let initDateAndTime = this.getDate(item.initialDatetime);
    let finalDateAndTime = this.getDate(item.finalDatetime);

    this.modalForm.get('modalEventId')?.setValue(item.id)
    this.modalForm.get('modalName')?.setValue(item.name);
    this.modalForm.get('modalDescription')?.setValue(item.description);
    this.modalForm.get('modalInitialDate')?.setValue(initDateAndTime.date);
    this.modalForm.get('modalInitialTime')?.setValue(initDateAndTime.time);
    this.modalForm.get('modalFinalDate')?.setValue(finalDateAndTime.date);
    this.modalForm.get('modalFinalTime')?.setValue(finalDateAndTime.time);
    this.modalForm.get('currentModalRef')?.setValue(this.modalService.open(content));
  }

  openDeleteModal(content: any, id: number, name: string) {
    this.modalDeleteEvent.get('nameModal')?.setValue(name);
    this.modalDeleteEvent.get('currentIdForRemove')?.setValue(id);
    this.currentModalRef = this.modalService.open(content);
  }


  constructor(
    private eventListService: EventListService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertModalService
  ) {
    this.modalForm = this.formBuilder.group({
      modalEventId: [null, Validators.required],
      modalName: [null, Validators.required],
      modalDescription: [null, Validators.required],
      modalInitialDate: [null, Validators.required],
      modalInitialTime: [null, Validators.required],
      modalFinalDate: [null, Validators.required],
      modalFinalTime: [null, Validators.required],
      currentModalRef: [null, Validators.required]
    });

    this.modalDeleteEvent = this.formBuilder.group({
      nameModal: [null, Validators.required],
      currentIdForRemove: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.getEvents();
  }

}
